package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Dialog extends JDialog implements ActionListener {

    private final JButton btnColor;
    private JSpinner mBox, vxBox, vyBox, xBox, yBox, rBox;
    private JButton btnAdd;
    private JButton btnCancel, btnClear, btnBW;
    private Masses mass;
    private Color color = Color.RED;
    private JPanel panel;
    public boolean clear = false;

    public Dialog(Frame parent, double xPos, double yPos) {
        super(parent, "Creator", true);
        //super(parent,parent.getGraphicsConfiguration(), true);
        Point loc = parent.getLocation();
        setLocation(loc.x + 80, loc.y + 80);
        //data = new double[][2]; // set to amount of data items
        panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        JLabel mLabel = new JLabel("Masa:");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(mLabel, gbc);
        mBox = new JSpinner(new SpinnerNumberModel(1000, 0, 90000000, 1000.0));
        ;
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 0;
        panel.add(mBox, gbc);
        JLabel colorLabel = new JLabel("Wypierz kolor:");
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(colorLabel, gbc);
        btnColor = new JButton("  ");
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 1;
        btnColor.setBackground(color);
        panel.add(btnColor, gbc);
        btnColor.addActionListener(this);
        JLabel xLabel = new JLabel("X:");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 2;
        panel.add(xLabel, gbc);
        xBox = new JSpinner(new SpinnerNumberModel(xPos, -750.0, 750.0, 0.2));
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 2;
        panel.add(xBox, gbc);
        JLabel yLabel = new JLabel("Y:");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 3;
        panel.add(yLabel, gbc);
        yBox = new JSpinner(new SpinnerNumberModel(yPos, -750.0, 750.0, 0.2));
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 3;
        panel.add(yBox, gbc);
        JLabel vxLabel = new JLabel("VX:");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 4;
        panel.add(vxLabel, gbc);
        vxBox = new JSpinner(new SpinnerNumberModel(4.0, -10.0, 10.0, 0.1));
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 4;
        panel.add(vxBox, gbc);
        JLabel vyLabel = new JLabel("VY:");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 5;
        panel.add(vyLabel, gbc);
        vyBox = new JSpinner(new SpinnerNumberModel(-4.0, -10.0, 10.0, 0.1));
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 5;
        panel.add(vyBox, gbc);
        JLabel spacer = new JLabel(" ");
        gbc.gridx = 2;
        gbc.gridy = 5;
        panel.add(spacer, gbc);
        JLabel rLabel = new JLabel("R:");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 6;
        panel.add(rLabel, gbc);
        rBox = new JSpinner(new SpinnerNumberModel(1.0, 1.0, 20.0, 1.0));
        gbc.gridwidth = 2;
        gbc.gridx = 1;
        gbc.gridy = 6;
        panel.add(rBox, gbc);
        btnBW = new JButton("Czarna dziura");
        btnBW.addActionListener(this);
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 7;
        panel.add(btnBW, gbc);
        btnClear = new JButton("Wyczyść");
        btnClear.addActionListener(this);
        gbc.gridx = 1;
        gbc.gridy = 7;
        panel.add(btnClear, gbc);
        getContentPane().add(panel);
        btnAdd = new JButton("Dodaj");
        btnAdd.addActionListener(this);
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 8;
        panel.add(btnAdd, gbc);
        btnCancel = new JButton("Anuluj");
        btnCancel.addActionListener(this);
        gbc.gridwidth = 1;
        gbc.gridx = 1;
        gbc.gridy = 8;
        panel.add(btnCancel, gbc);
        getContentPane().add(panel);
        pack();
    }


    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == btnColor) {
            Color initialcolor = Color.RED;
            color = JColorChooser.showDialog(this, "Select a color", initialcolor);
            btnColor.setBackground(color);
        } else if (ae.getSource() == btnAdd) {
            mass = new Masses(
                    "mass",
                    (Double) mBox.getValue() / 1989100000,
                    (Double) xBox.getValue(),
                    (Double) yBox.getValue(),
                    (Double) 0.0192761582256879,
                    (Double) vxBox.getValue(),
                    (Double) vyBox.getValue(),
                    (Double) 0.391700036358566,
                    0,
                    0,
                    0,
                    (Double) rBox.getValue(),
                    color);
            dispose();
        } else if (ae.getSource() == btnBW) {
            mass = new Masses(
                    "BW",
                    (Double) 1500.0,
                    (Double) xBox.getValue(),
                    (Double) yBox.getValue(),
                    (Double) 0.0192761582256879,
                    (Double) 0.0,
                    (Double) 0.0,
                    (Double) 0.0,
                    0,
                    0,
                    0,
                    (Double) 30.0,
                    Color.BLACK);
            dispose();
        } else if (ae.getSource() == btnClear) {
            mass = null;
            clear = true;
            dispose();
        } else {
            mass = null;
            dispose();
        }
    }

    public JPanel getPanel() {
        return panel;
    }

    public Masses run() {
        this.setVisible(true);
        return mass;
    }
}
