package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static com.sun.java.accessibility.util.AWTEventMonitor.addMouseListener;

public class Draw {

    private JFrame frame;
    public static Canvas canvas;

    private String title;
    Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
    final int width = (int) size.getWidth();
    final int height = (int) size.getHeight();

    public Draw(String title) {
        this.title = title;

        createDisplay();
    }


    private void createDisplay()
    {
        frame = new JFrame(title);
        frame.setSize(width, height);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(true);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(width, height));
        canvas.setBackground(Color.BLACK);
        canvas.setMaximumSize(new Dimension(width, height));
        canvas.setMinimumSize(new Dimension(width, height));
        canvas.setFocusable(false);

        frame.add(canvas);
    }

    public static void drawOval(int x, int y, double radius, int m, Color color, String name) {
        Graphics g = canvas.getGraphics();
        int r = (int) radius;//(radius + 12 * m);
        if (name.equals("BW")) {
            g.setColor(Color.WHITE);
            g.fillOval((int) (x - r) - 1, (int) (y - r) - 1, (2 * r) + 2, (2 * r) + 2);
        }
        g.setColor(color);


        g.fillOval((int) x - r, (int) y - r, 2 * r, 2 * r);

    }

    public Canvas getCanvas() {
        return canvas;
    }

    public JFrame getFrame() {
        return frame;
    }

}
