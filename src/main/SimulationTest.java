package main;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class SimulationTest {

    @Test
    void shouldUpdatePositionVectors() {
        ArrayList<Masses> masses = new ArrayList<Masses>();
        masses.add(new Masses(
                "m2",
                1.65956463e-7,
                1,
                -0.698844725464528,
                0.0192761582256879,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        double xPosAfterUpdate = masses.get(0).x + masses.get(0).vx * 0.008;
        masses = Simulation.updatePositionVectors(masses, 0.008);
        assertEquals(xPosAfterUpdate, masses.get(0).x);


    }

    @Test
    void shouldUpdateVelocityVectors() {
        ArrayList<Masses> masses = new ArrayList<Masses>();
        masses.add(new Masses(
                "m2",
                1.65956463e-7,
                1,
                -0.698844725464528,
                0.0192761582256879,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        masses.add(new Masses(
                "BW",
                1.65956463e-7,
                1,
                -0.698844725464528,
                0.0192761582256879,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        double vxPosAfterUpdate = masses.get(0).vx + masses.get(0).ax * 0.008;
        masses = Simulation.updateVelocityVectors(masses, 0.008);
        assertEquals(vxPosAfterUpdate, masses.get(0).vx);
        assertEquals(0, masses.get(1).vx);
    }

    @Test
    void shouldUpdateAccelerationVectors() {
        ArrayList<Masses> masses = new ArrayList<Masses>();
        masses.add(new Masses(
                "m2",
                1.0,
                1.0,
                1.0,
                0.1,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        masses.add(new Masses(
                "BW",
                1.5,
                1.5,
                1.5,
                0.3,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        double dx = 1.5 - 1.0;
        double dy = 1.5 - 1.0;
        double dz = 0.3 - 0.1;

        double distSq = dx * dx + dy * dy + dz * dz;

        double f = (double) ((39.5 * 1.5) / (distSq * Math.sqrt(distSq + 0.15)));
        double axAfterUpdate = dx * f;

        masses = Simulation.updateAccelerationVectors(masses, 39.5, 0.15);
        assertEquals(axAfterUpdate, masses.get(0).ax);
    }

    @Test
    void shouldNotDetectCollision() {
        ArrayList<Masses> masses = new ArrayList<Masses>();
        masses.add(new Masses(
                "m2",
                1.0,
                1.0,
                1.0,
                0.1,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        masses.add(new Masses(
                "m2",
                1.5,
                1.5,
                1.5,
                0.3,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        int collision = Simulation.collisionDetector(masses, 70);
        assertEquals(-1, collision);
    }

    @Test
    void shouldDetectCollision() {
        ArrayList<Masses> masses = new ArrayList<Masses>();
        masses.add(new Masses(
                "m2",
                1.0,
                1.0,
                1.0,
                0.1,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        masses.add(new Masses(
                "m2",
                1.3,
                1.0,
                1.0,
                0.3,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        int collision = Simulation.collisionDetector(masses, 70);
        assertEquals(0, collision);

        masses.clear();
        masses.add(new Masses(
                "m2",
                1.0,
                1.0,
                1.0,
                0.1,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        masses.add(new Masses(
                "m2",
                0.4,
                1.0,
                1.0,
                0.3,
                2,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        collision = Simulation.collisionDetector(masses, 70);
        assertEquals(1, collision);

    }

}
