package main;

import java.awt.*;

public class Masses {


    final String name;
    final double m;
    double x;
    double y;
    double z;
    double vx;
    double vy;
    double vz;
    double ax;
    double ay;
    double az;
    double r;
    Color color;

    public Masses(String name, double m, double x, double y, double z, double vx, double vy, double vz, double ax, double ay, double az, double r, Color color) {
        this.name = name;
        this.m = m;
        this.x = x;
        this.y = y;
        this.z = z;
        this.vx = vx;
        this.vy = vy;
        this.vz = vz;
        this.ax = ax;
        this.ay = ay;
        this.az = az;
        this.r = r;
        this.color = color;
    }
}
