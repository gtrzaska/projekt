package main;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class App implements Runnable {
    private boolean running = false;
    double g = 39.5;
    double dt = 0.005;
    double softeningConstant = 0.15;
    int scale = 70;
    Draw screen;
    ArrayList<Masses> masses = new ArrayList<Masses>();

    public App() {
        masses.add(new Masses(
                "m1",
                1,
                -1.50324727873647e-6,
                -3.93762725944737e-6,
                -4.86567877183925e-8,
                3.1669325898331e-5,
                -0.85489559263319e-6,
                -1.90076642683254e-7,
                0,
                0,
                0,
                15,
                Color.YELLOW));
        masses.add(new Masses(
                "m2",
                1.65956463e-7,
                -0.168003526072526,
                -0.698844725464528,
                0.0192761582256879,
                -7.2077847105093,
                -1.76778886124455,
                0.391700036358566,
                0,
                0,
                0,
                7,
                Color.ORANGE));
        masses.add(new Masses(
                "m3",
                3.0024584e-6,
                0.848778995445634,
                0.747796691108466,
                -3.22953591923124e-5,
                -4.85085525059392,
                4.09601538682312,
                -0.000258553333317722,
                0,
                0,
                0,
                8,
                Color.BLUE));
    }


    @Override
    public void run() {
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        final int width = (int) size.getWidth();
        final int height = (int) size.getHeight();

        screen = new Draw("GravitySimulator");

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                screen.getCanvas().getGraphics().clearRect(0, 0, width, height);

                masses = Simulation.updatePositionVectors(masses, dt);
                masses = Simulation.updateAccelerationVectors(masses, g, softeningConstant);
                masses = Simulation.updateVelocityVectors(masses, dt);
                int collision = Simulation.collisionDetector(masses, scale);
                if (collision >= 0) {
                    masses.remove(collision);
                }

                for (int i = 0; i < masses.size(); i++) {
                    double x = width / 2 + masses.get(i).x * scale;
                    double y = height / 2 + masses.get(i).y * scale;
                    Draw.drawOval((int) x, (int) y, masses.get(i).r * scale / 55, (int) masses.get(i).m, masses.get(i).color, masses.get(i).name);
                }
            }
        }, 0, 16);

        screen.getCanvas().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                System.out.print(me.getX() + " - " + width / 2 + " : " + me.getY() + " - " + height / 2);
                System.out.print("\n" + (me.getX() - width / 2) / scale + " : " + (me.getY() - height / 2) / scale);
                Dialog dlg = new Dialog(screen.getFrame(), (me.getX() - width / 2.0) / scale, (me.getY() - height / 2.0) / scale);
                Masses newMasses = dlg.run();

                if (newMasses != null) {
                    masses.add(newMasses);
                } else {
                    if (dlg.clear) {
                        masses.clear();
                    }
                }

            }
        });

        screen.getCanvas().addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if ((int) e.getWheelRotation() < 0) {
                    scale += 4;
                } else {
                    scale -= 4;
                }
            }
        });


        screen.getCanvas().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                System.out.println("Key pressed code=" + e.getKeyCode() + ", char=" + e.getKeyChar());
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });


    }

    public synchronized void start() {
        if (running)
            return;
        running = true;
        Thread thread = new Thread(this);
        thread.start();

    }

}
