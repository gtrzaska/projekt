package main;

import java.awt.*;
import java.util.ArrayList;

public class Simulation {

    public static ArrayList<Masses> updatePositionVectors(ArrayList<Masses> masses, double dt) {
        int massesLen = masses.size();

        for (int i = 0; i < massesLen; i++) {
            Masses massI = masses.get(i);

            massI.x += massI.vx * dt;
            massI.y += massI.vy * dt;
            massI.z += massI.vz * dt;
        }

        return masses;
    }

    public static ArrayList<Masses> updateVelocityVectors(ArrayList<Masses> masses, double dt) {
        int massesLen = masses.size();
        for (int i = 0; i < massesLen; i++) {
            Masses massI = masses.get(i);
            if (massI.name.equals("BW")) {
                massI.vx = 0;
                massI.vy = 0;
                massI.vz = 0;
            } else {
                massI.vx += massI.ax * dt;
                massI.vy += massI.ay * dt;
                massI.vz += massI.az * dt;
            }
        }
        return masses;
    }

    public static ArrayList<Masses> updateAccelerationVectors(ArrayList<Masses> masses, double g, double softeningConstant) {
        int massesLen = masses.size();

        for (int i = 0; i < massesLen; i++) {
            double ax = 0;
            double ay = 0;
            double az = 0;

            Masses massI = masses.get(i);

            for (int j = 0; j < massesLen; j++) {
                if (i != j) {
                    Masses massJ = masses.get(j);

                    double dx = massJ.x - massI.x;
                    double dy = massJ.y - massI.y;
                    double dz = massJ.z - massI.z;

                    double distSq = dx * dx + dy * dy + dz * dz;

                    double f = (double) ((g * massJ.m) / (distSq * Math.sqrt(distSq + softeningConstant)));

                    ax += dx * f;
                    ay += dy * f;
                    az += dz * f;
                }
            }

            massI.ax = ax;
            massI.ay = ay;
            massI.az = az;

        }
        return masses;
    }

    Dimension size = Toolkit.getDefaultToolkit().getScreenSize();

    public static int collisionDetector(ArrayList<Masses> masses, int scale) {
        int massesLen = masses.size();
        double distance;
        for (int i = 0; i < massesLen; i++) {
            Masses massI = masses.get(i);

            for (int j = 0; j < massesLen; j++) {

                if (i != j) {
                    Masses massJ = masses.get(j);
                    distance = Math.sqrt((massJ.x * scale - massI.x * scale) * (massJ.x * scale - massI.x * scale) + (massJ.y * scale - massI.y * scale) * (massJ.y * scale - massI.y * scale));
                    double rr = massI.r + massJ.r;
                    if (distance < rr) {
                        System.out.print(" Distance " + massI.name + " - " + massJ.name + " = " + distance);
                        if (massI.m < massJ.m) {
                            return i;
                        } else {
                            return j;
                        }

                    }

                }
            }
        }
        return -1;
    }


}
