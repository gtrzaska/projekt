package main;

import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class DialogTest extends JDialog {

    @Test
    void shouldCreateDialog() {
        Dialog dialog = new Dialog(new Frame(), 10, 10);

        String panel = dialog.getPanel().toString();
        dialog.dispose();
        assertNotNull(panel);

    }
}
